package zonk

import scala.util.Random
import scala.collection.immutable.IndexedSeq

object Zonk extends App {
  trait Game
  case class Init(carSlot: Int) extends Game {
    require(carSlot >= 0 && carSlot <= 2)
  }
  case class PickedUpSlot(carSlot: Int, pickedUpSlot: Int) extends Game {
    require(carSlot >= 0 && carSlot <= 2)
    require(pickedUpSlot >= 0 && pickedUpSlot <= 2)
  }
  case class GoatShown(carSlot: Int, pickedUpSlot: Int, goatShownSlot: Int) extends Game {
    require(carSlot >= 0 && carSlot <= 2)
    require(pickedUpSlot >= 0 && pickedUpSlot <= 2)
    require(goatShownSlot >= 0 && goatShownSlot <= 2)
    require(goatShownSlot != pickedUpSlot && goatShownSlot != carSlot)
  }

  class Player(val change: Boolean) {
    val random = Random

    def init: Init = Init(random.nextInt(3))

    def pickFirstSlot(init: Init): PickedUpSlot = {
      PickedUpSlot(init.carSlot, random.nextInt(3))
    }

    def showAGoat(pickedUpSlot: PickedUpSlot): GoatShown = {
      val allSlots: Range = (0 until 3)
      val allButCarAndPicked: IndexedSeq[Int] = allSlots.filter(i => {
        i != pickedUpSlot.carSlot && i != pickedUpSlot.pickedUpSlot
      })
      GoatShown(pickedUpSlot.carSlot, pickedUpSlot.pickedUpSlot, random.shuffle(allButCarAndPicked).head)
    }

    def finalCall(goatShown: GoatShown): Boolean = {
      val finalSlot = if (!change) {
        goatShown.pickedUpSlot
      } else {
        val allSlots: Range = (0 until 3)
        val others: IndexedSeq[Int] = allSlots.filter(i => {
          i != goatShown.goatShownSlot && i != goatShown.pickedUpSlot
        })
        require(others.size == 1)
        val other = others.head
        require(other != goatShown.pickedUpSlot && other != goatShown.goatShownSlot)
        other
      }

      return finalSlot == goatShown.carSlot
    }

    def play: Boolean = {
      val pickedUpSlot = pickFirstSlot(init)
      val goatShown = showAGoat(pickedUpSlot)
      val result = finalCall(goatShown)
      result
    }
  }

  override def main(args: Array[String]): Unit = {
    val iterations = 100000
    def test(change: Boolean) {
      var successCnt: Int = 0
      val player = new Player(change)
      for (i <- 1 to iterations) {
        if (player.play) {
          successCnt = successCnt + 1
        }
      }
      val pct = 100 * successCnt / iterations
      println(s"Player(change=$change) won in $successCnt cases in $iterations ($pct%)")
    }
    test(true)
    test(false)
    test(true)
    test(false)
    test(true)
    test(false)
  }
}

